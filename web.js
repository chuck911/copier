const Koa = require('koa')
const fs = require('fs-extra')
const serve = require('koa-static')
const {urlToPath} = require('./lib')
const app = new Koa();

const htmlDir = './html/'

app.use(serve(__dirname+'/assets/'))

app.use(async ctx => {
	console.log(ctx.url)
	const path = htmlDir + urlToPath(ctx.url)
	// console.log(path)
	if (fs.existsSync(path)) {
		ctx.type = 'text/html; charset=utf-8'
		ctx.body = fs.createReadStream(path)
	}
});

app.listen(3111);