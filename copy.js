const got = require('got')
const charset = require('charset')
const iconv = require('iconv-lite')
const cheerio = require('cheerio')
const fs = require('fs-extra')
const {urlToPath} = require('./lib')

const site = 'http://old.huoyuanzhijia.com'
const host = new URL(site).host

async function getHtml(url) {
	const res = await got.get(url, {responseType: 'buffer'})
	const encoding = charset(res.headers, res.body, 2048)
	if (encoding == 'utf8') {
		return res.body.toString()
	} else {
		return iconv.decode(res.body, encoding)
	}
}

async function fetch(site, urlPath) {
	console.log('fetch',urlPath)
	const path = urlToPath(urlPath)
	const url = site+urlPath
	let html
	try {
		html = await getHtml(url)
	} catch (error) {
		return null
	}
	
	const CHARTSET_RE = /charset\s{0,10}=\s{0,10}['"]? {0,10}([\w\-]{1,100})/ig;
	html = html.replace(CHARTSET_RE, 'charset=utf-8')
	const htmlFile = './html/'+path
	console.log(htmlFile)
	// if (await fs.pathExists(htmlFile)) return

	await fs.outputFile(htmlFile, html)
	const $ = cheerio.load(html)
	const links = $('a').map((i, el)=>$(el).attr('href')).get()
	let urls = links.map(link => new URL(link, site))
		.filter(url => url.host==host)
		.map(url => url.pathname+url.search)
	let iframes = $('iframe[src]').map((i, el)=>$(el).attr('src')).get()
		.map(link => new URL(link, site)).filter(url => url.host==host).map(url => url.pathname)
	urls = urls.concat(iframes)
	urls = [...new Set(urls)]
	let rels = $('link[href]').map((i, el)=>$(el).attr('href')).get()
		.map(link => new URL(link, site)).filter(url => url.host==host).map(url => url.pathname)
	let images = $('img[src]').map((i, el)=>$(el).attr('src')).get()
		.map(link => new URL(link, site)).filter(url => url.host==host).map(url => url.pathname)
	let assets = rels.concat(images)
	assets = [...new Set(assets)]
	return {urls, assets}
}

async function fetchAsset(site, urlPath) {
	console.log('assets',urlPath)
	const file = './assets/'+urlPath
	const res = await got(site+urlPath, {responseType: 'buffer'})
	await fs.outputFile(file, res.body)
}


;(async ()=>{
	const resources = await fetch(site, '/')
	console.log(resources)
	// for (const url of resources.urls) {
	// 	await fetch(site, url)
	// }
	for (const url of resources.assets) {
		await fetchAsset(site, url)
	}
})()
