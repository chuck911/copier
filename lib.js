const md5 = require('md5')

function urlToPath(urlPath) {
	const ext = '.html'
    const hash = md5(urlPath)
	return hash.slice(0,2)+'/'+hash.slice(2,4)+'/'+hash.slice(4)+ext
}

module.exports = {urlToPath}