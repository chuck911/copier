const got = require('got')
const charset = require('charset')
const iconv = require('iconv-lite')
const cheerio = require('cheerio')
const fs = require('fs-extra')
const Queue = require('bee-queue')
const Redis = require("ioredis")
const redis = new Redis()
const {urlToPath} = require('./lib')

const queue = new Queue('old.huoyuanzhijia')
const urlSet = 'old.huoyuanzhijia.com:set'

const site = 'http://old.huoyuanzhijia.com'
const host = new URL(site).host

async function getHtml(url) {
	const res = await got.get(url, {responseType: 'buffer'})
	const encoding = charset(res.headers, res.body, 2048)
	if (encoding == 'utf8') {
		return res.body.toString()
	} else {
		return iconv.decode(res.body, encoding)
	}
}

async function fetch(site, urlPath) {
	console.log('fetch',urlPath)
    const path = urlToPath(urlPath)
    const htmlFile = './html/'+path
	if (await fs.pathExists(htmlFile)) return
	const url = site+urlPath
	let html
	try {
		html = await getHtml(url)
	} catch (error) {
		return null
	}
	
	const CHARTSET_RE = /charset\s{0,10}=\s{0,10}['"]? {0,10}([\w\-]{1,100})/ig;
	html = html.replace(CHARTSET_RE, 'charset=utf-8')

	await fs.outputFile(htmlFile, html)
	const $ = cheerio.load(html)
	const links = $('a').map((i, el)=>$(el).attr('href')).get()
	let urls = links.map(link => new URL(link, site))
		.filter(url => url.host==host)
		.map(url => url.pathname+url.search)
	let iframes = $('iframe[src]').map((i, el)=>$(el).attr('src')).get()
		.map(link => new URL(link, site)).filter(url => url.host==host).map(url => url.pathname)
	urls = urls.concat(iframes)
	urls = [...new Set(urls)]
	let rels = $('link[href]').map((i, el)=>$(el).attr('href')).get()
		.map(link => new URL(link, site)).filter(url => url.host==host).map(url => url.pathname)
	let images = $('img[src]').map((i, el)=>$(el).attr('src')).get()
		.map(link => new URL(link, site)).filter(url => url.host==host).map(url => url.pathname)
	let assets = rels.concat(images)
	assets = [...new Set(assets)]
	return {urls, assets}
}

async function fetchAsset(site, urlPath) {
	console.log('assets',urlPath)
	const file = './assets/'+urlPath
	const res = await got(site+urlPath, {responseType: 'buffer'})
    await fs.outputFile(file, res.body)
    return urlPath
}

async function addJob(url, type) {
    if (await redis.sismember(urlSet, url)) return

    const job = queue.createJob({url, type})
    await job.timeout(10000).retries(2).save()
    await redis.sadd(urlSet, url)
}


;(async ()=>{
	// const resources = await fetch(site, '/')
	await queue.createJob({url:'/', type:'html'}).save()
})()

queue.process(async job => {
    if (job.data.type=='assets') {
        return await fetchAsset(site, job.data.url)
    }
    const resources = await fetch(site, job.data.url)
    if (!resources) return

    for (const url of resources.urls) {
        await addJob(url, 'html')
    }
    for (const url of resources.assets) {
        await addJob(url, 'assets')
    }
})
